using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using FluentAssertions;
using MoreLinq;
using QueueApp.Enums;
using Xunit;

namespace QueueApp.Tests
{
    public class MultiThreadedQueueTests
    {
        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldEnqueueSingleItem(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);

            service.Enqueue(5);

            service.Count.Should().Be(1);
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldEnqueueMultipleItems(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);

            service.Enqueue(new[] {5, 10});

            service.Count.Should().Be(2);
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldReturnEmtpyIfNoItems(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);

            var result = service.GetAllItems();

            result.Should().BeEmpty();
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldReturnAllItems(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);
            service.Enqueue(new[] {5, 10});

            var result = service.GetAllItems().ToList();

            result.Should().NotBeEmpty();
            result.Should().HaveCount(2);

            if (queueType == QueueType.Lifo)
            {
                result[0].Should().Be(5);
                result[1].Should().Be(10);
            }

            if (queueType == QueueType.Fifo)
            {
                result[0].Should().Be(10);
                result[1].Should().Be(5);
            }
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldThrowIfNoItemsToDequeue(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);

            Action dequeueAction = () => service.Dequeue(DequeueType.Immediate);
            Action dequeueAllAction = () => service.DequeueAll();

            dequeueAction.Should().Throw<InvalidOperationException>();
            dequeueAllAction.Should().Throw<InvalidOperationException>();
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldDequeueSingleItem(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);
            service.Enqueue(new[] {5, 10});

            var result = service.Dequeue(DequeueType.Immediate);

            service.Count.Should().Be(1);

            if (queueType == QueueType.Lifo)
            {
                result.Should().Be(5);
            }

            if (queueType == QueueType.Fifo)
            {
                result.Should().Be(10);
            }
        }
        
        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldDequeueAllItems(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);
            service.Enqueue(new[] {5, 10});

            var result = service.DequeueAll().ToList();

            result.Should().HaveCount(2);

            if (queueType == QueueType.Lifo)
            {
                result[0].Should().Be(5);
                result[1].Should().Be(10);
            }

            if (queueType == QueueType.Fifo)
            {
                result[0].Should().Be(10);
                result[1].Should().Be(5);
            }
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void SingleThreadShouldBeAbleToEnqueue(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);

            var items = Enumerable.Range(1, 10);

            var waitHandler = new WaitHandle[]
            {
                new AutoResetEvent(false)
            };

            var thread1 = new Thread(() =>
            {
                items.ForEach(item => service.Enqueue(item));
                ((AutoResetEvent)waitHandler[0]).Set();
            });

            thread1.Start();

            var status = WaitHandle.WaitAll(waitHandler);
            status.Should().BeTrue();

            service.Count.Should().Be(10);

            var queueItems = service.GetAllItems();
            queueItems.Should().BeEquivalentTo(items);
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void MultipleThreadsShouldBeAbleToEnqueue(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);

            var items1 = Enumerable.Range(1, 10).ToList();
            var items2 = Enumerable.Range(11, 10).ToList();
            
            var waitHandler = new WaitHandle[]
            {
                new AutoResetEvent(false),
                new AutoResetEvent(false)
            };

            var thread1 = new Thread(() =>
            {
                items1.ForEach(item => service.Enqueue(item));
                ((AutoResetEvent) waitHandler[0]).Set();
            });
            thread1.Start();

            var thread2 = new Thread(() =>
            {
                items2.ForEach(item => service.Enqueue(item));
                ((AutoResetEvent) waitHandler[1]).Set();
            });
            thread2.Start();

            var status = WaitHandle.WaitAll(waitHandler);
            status.Should().BeTrue();

            service.Count.Should().Be(20);
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldThrowImmediatelyIfNothingtoDequeue(QueueType queueType)
        {
            var service = new MultiThreadedQueue<int>(queueType);

            Action action = () => service.Dequeue(DequeueType.Immediate);

            action.Should().Throw<InvalidOperationException>();
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldWaitToDequeue(QueueType queueType)
        {
            const int waitTime = 2000;
            var stopwatch = new Stopwatch();
            var service = new MultiThreadedQueue<int>(queueType);

            var waitHandler = new WaitHandle[]
            {
                new AutoResetEvent(false),
                new AutoResetEvent(false)
            };

            var t1 = new Thread(() =>
            {
                stopwatch.Start();
                service.Dequeue(DequeueType.Wait);
                ((AutoResetEvent)waitHandler[0]).Set();
                stopwatch.Stop();
            });
            t1.Start();

            var t2 = new Thread(() =>
            {
                // Simulate late availability of data after 2 sec
                Thread.Sleep(waitTime);

                service.Enqueue(Enumerable.Range(1, 10));
                ((AutoResetEvent)waitHandler[1]).Set();
            });
            t2.Start();

            var status = WaitHandle.WaitAll(waitHandler);

            status.Should().BeTrue();
            var items = service.GetAllItems();
            items.Should().HaveCount(9);
            stopwatch.ElapsedMilliseconds.Should().BeGreaterOrEqualTo(waitTime);
        }

        [Theory]
        [InlineData(QueueType.Lifo)]
        [InlineData(QueueType.Fifo)]
        public void ShouldTimeoutWhenDequeing(QueueType queueType)
        {
            const int dataAvailabilityTime = 3000;
            var stopwatch = new Stopwatch();
            var service = new MultiThreadedQueue<int>(queueType);

            var waitHandler = new WaitHandle[]
            {
                new AutoResetEvent(false),
                new AutoResetEvent(false)
            };

            var t1 = new Thread(() =>
            {
                stopwatch.Start();
                Action action = () => service.Dequeue(DequeueType.Timeout);

                action.Should().Throw<InvalidOperationException>();
                ((AutoResetEvent)waitHandler[0]).Set();
                stopwatch.Stop();
            });
            t1.Start();

            var t2 = new Thread(() =>
            {
                // Simulate late availability of data after 3 sec
                Thread.Sleep(dataAvailabilityTime);

                service.Enqueue(Enumerable.Range(1, 10));
                ((AutoResetEvent)waitHandler[1]).Set();
            });
            t2.Start();

            var status = WaitHandle.WaitAll(waitHandler);

            status.Should().BeTrue();
            stopwatch.ElapsedMilliseconds.Should().BeGreaterOrEqualTo(2000).And.BeLessThan(dataAvailabilityTime);
        }
    }
}