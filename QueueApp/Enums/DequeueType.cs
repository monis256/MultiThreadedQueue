namespace QueueApp.Enums
{
    public enum DequeueType
    {
        Immediate,
        Wait,
        Timeout
    }
}