namespace QueueApp.Enums
{
    public enum QueueType
    {
        Lifo,
        Fifo
    }
}