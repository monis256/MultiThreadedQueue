﻿using System.Collections.Generic;
using QueueApp.Enums;

namespace QueueApp
{
    public interface IMultiThreadedQueue<T>
    {
        void Enqueue(T item);
        void Enqueue(IEnumerable<T> items);
        T Dequeue(DequeueType dequeueType);
        IEnumerable<T> DequeueAll();
        IEnumerable<T> GetAllItems();
    }
}