using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MoreLinq;
using QueueApp.Enums;

namespace QueueApp
{
    internal sealed class MultiThreadedQueue<T> : IMultiThreadedQueue<T>
    {
        private readonly LinkedList<T> _queue;
        private readonly QueueType _queueType;
        private readonly AutoResetEvent _waitHandler;
        private readonly object _lock = new object();

        public MultiThreadedQueue(QueueType queueType)
        {
            _queueType = queueType;
            _waitHandler = new AutoResetEvent(false);
            _queue = new LinkedList<T>();
        }

        public int Count { get; private set; }

        public void Enqueue(T item)
        {
            AddItem(item);
        }

        public void Enqueue(IEnumerable<T> items)
        {
            items.ForEach(AddItem);
        }

        public T Dequeue(DequeueType dequeueType)
        {
            var waitTime = GetWaitTime(dequeueType);

            if (_waitHandler.WaitOne(waitTime))
            {
                lock (_lock)
                {
                    ValidateOperation();

                    var item = _queue.First.Value;

                    _queue.RemoveFirst();
                    UpdateItemCount();

                    return item;
                }
            }
            throw new InvalidOperationException("No Items to Dequeue");
        }

        public IEnumerable<T> DequeueAll()
        {
            ValidateOperation();

            var allItems = new T[_queue.Count];
            _queue.CopyTo(allItems, 0);
            _queue.Clear();

            UpdateItemCount();

            return allItems;
        }

        public IEnumerable<T> GetAllItems()
        {
            return _queue.AsEnumerable();
        }

        private int GetWaitTime(DequeueType dequeueType)
        {
            switch (dequeueType)
            {
                case DequeueType.Immediate:
                    return 0;
                case DequeueType.Wait:
                    return -1;
                case DequeueType.Timeout:
                    return 2000;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dequeueType), dequeueType, null);
            }
        }

        private void ValidateOperation()
        {
            if (!_queue.Any())
            {
                throw new InvalidOperationException("No Items to Dequeue");
            }
        }

        private void AddItem(T item)
        {
            lock (_lock)
            {
                switch (_queueType)
                {
                    case QueueType.Lifo:
                        _queue.AddLast(item);
                        break;
                    case QueueType.Fifo:
                        _queue.AddFirst(item);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                UpdateItemCount();
                _waitHandler.Set();
            }
        }

        private void UpdateItemCount()
        {
            Count = _queue.Count;
        }
    }
}