Pre-requisites

- [.NET Core 2.1](https://www.microsoft.com/net/download/dotnet-core/sdk-2.1.300-preview1)

- This is a Multi-threaded custom queue supporting FIFO & LIFO developed in C#
- Clone this repo and run it using Visual Studio
- The unit tests demonstrates all operations of the queue